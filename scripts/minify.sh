#!/bin/sh
# awk strips leading and heading whitespace per line
# tr replaces newline characters with spaces
# sed removes the whitespace around these characters: {}();=:>,?
# the tr at the end removes the single trailing newline created by sed
awk '{$1=$1};1' | tr -s '\n' ' ' | sed 's/[[:space:]]*\([{}();=:>,?]\)[[:space:]]*/\1/g' | tr -d '\n'


#!/usr/bin/env python3
import fileinput

remove_spaces_surrounding = "{}();=:>,?"

for line in fileinput.input():
    # removes leading and trailing whitespace for this line
    no_whitespace = line.strip()

    # adds a space after @annotation to prevent there not being any whitespace after the annotation
    if no_whitespace.startswith("@"):
        no_whitespace += " "

    # removes leading and trailing spaces around the characters in remove_spaces_surrounding
    for char in remove_spaces_surrounding:
        no_whitespace = no_whitespace.replace(" " + char, char)
        no_whitespace = no_whitespace.replace(char + " ", char)

    # print without adding newline characters
    print(no_whitespace, end='')
